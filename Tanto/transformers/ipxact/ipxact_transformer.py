'''
Created on Jun 28, 2012

Copyright 2012 Verilab, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
@author: gmcgregor
'''

from logging import info, debug, warning
from tanto.tanto import TantoTransformer

class IpxactElement:
    def __init__(self, data, ns):
        self.data = data
        self.ns = ns
        
    def __getitem__(self, item):
        debug('IP-XACTElement::__getitem__ called with %s' % item)    
        return self.findall(item)               
      
    def findall(self, item, ns=None):
        if not ns:
            ns = self.ns
        items = self.data.findall('./' + ns.substitute(tag=item))    
        if not items:
            try:
                warning('IP-XACT element lookup : %s has no sub-element %s' % (self.data.find('./'+ self.ns.substitute(tag='name')).text, item))
            except:
                warning('IP-XACT element lookup : no sub-element %s' % (item))

        wrapped_items = [IpxactElement(item, ns) for item in items]
        return wrapped_items
              
    def __getattr__(self, attr):
        try:
            debug('IP-XACTElement::__getattr__ %s called with %s' % (self.data.find('./'+ self.ns.substitute(tag='name')).text, attr))
        except:
            debug('IP-XACTElement::__getattr__ unknown called with %s' %  attr)

        item = self.data.find('./' + self.ns.substitute(tag=attr))  
        if not item.text:
            try:
                warning('IP-XACT element lookup : %s has no sub-element %s' % (self.data.find('./'+ self.ns.substitute(tag='name')).text, attr))
            except:
                warning('IP-XACT element lookup : no sub-element %s' % (attr))
             
        return item.text.strip()
        
class IpxactTransformer(TantoTransformer):     
    def __getitem__(self, item):
        debug('IP-XACT Transformer::__getitem__ called with %s' % item)        
        items = self.data.root.findall('./' + self.data.spirit.substitute(tag=item))
        
        wrapped_items = [IpxactElement(item, self.data.spirit) for item in items]
        return wrapped_items
    
    def __getattr__(self, attr):
        try:
            debug('IP-XACTTransformer::__getattr__ %s called with %s' % (self.data.root.find('./'+ self.data.spirit.substitute(tag='name')).text, attr))
        except:
            debug('IP-XACTTransformer::__getattr__ unknown called with %s' %  attr)

        item = self.data.root.find('./' + self.data.spirit.substitute(tag=attr))   
        return item.text.strip()
