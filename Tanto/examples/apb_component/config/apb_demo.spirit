<?xml version="1.0" encoding="UTF-8" ?>
<spirit:component xmlns:spirit="http://www.spiritconsortium.org/XMLSchema/SPIRIT/1.5"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.spiritconsortium.org/XMLSchema/SPIRIT/1.5 http://www.accellera.org/XMLSchema/SPIRIT/1.5/index.xsd">
	<spirit:vendor>verilab.com</spirit:vendor>
	<spirit:library>Verilab</spirit:library>
	<spirit:name>apb_demo</spirit:name>
	<spirit:version>1.00</spirit:version>
	<spirit:memoryMaps>
		<spirit:memoryMap>
			<spirit:name>demo_map1</spirit:name>
			<spirit:addressBlock>
				<spirit:name>counter0</spirit:name>
				<spirit:baseAddress>0x1000</spirit:baseAddress>
				<spirit:range>64</spirit:range>
				<spirit:width>32</spirit:width>
			<spirit:register>
					<spirit:name>control</spirit:name>
					<spirit:description>Control register</spirit:description>
					<spirit:addressOffset>0x0</spirit:addressOffset>
					<spirit:size>32</spirit:size>
					<spirit:access>read-write</spirit:access>
					<spirit:field>
						<spirit:name>start</spirit:name>
						<spirit:description>Enables the counter</spirit:description>
						<spirit:bitOffset>0</spirit:bitOffset>
						<spirit:bitWidth>1</spirit:bitWidth>
					</spirit:field>
          <spirit:field>
            <spirit:name>stop</spirit:name>
            <spirit:description>Pauses the counter</spirit:description>
            <spirit:bitOffset>1</spirit:bitOffset>
            <spirit:bitWidth>1</spirit:bitWidth>
          </spirit:field>
          <spirit:field>
            <spirit:name>reset</spirit:name>
            <spirit:description>Resets the count value</spirit:description>
            <spirit:bitOffset>2</spirit:bitOffset>
            <spirit:bitWidth>1</spirit:bitWidth>
          </spirit:field>							
		</spirit:register>
				<spirit:register>
					<spirit:name>status</spirit:name>
					<spirit:description>Status register</spirit:description>
					<spirit:addressOffset>0x4</spirit:addressOffset>
					<spirit:size>32</spirit:size>
					<spirit:volatile>true</spirit:volatile>
					<spirit:access>read-only</spirit:access>
          <spirit:field>
            <spirit:name>running</spirit:name>
            <spirit:description>Counter is enabled</spirit:description>
            <spirit:bitOffset>0</spirit:bitOffset>
            <spirit:bitWidth>1</spirit:bitWidth>
            <spirit:volatile>true</spirit:volatile>
          </spirit:field>
          <spirit:field>
            <spirit:name>paused</spirit:name>
            <spirit:description>Counter is paused</spirit:description>
            <spirit:bitOffset>1</spirit:bitOffset>
            <spirit:bitWidth>1</spirit:bitWidth>
            <spirit:volatile>true</spirit:volatile>
          </spirit:field>          										
					<spirit:field>
						<spirit:name>zero</spirit:name>
						<spirit:description>Indicates if the count is zero</spirit:description>
						<spirit:bitOffset>2</spirit:bitOffset>
						<spirit:bitWidth>1</spirit:bitWidth>
						<spirit:volatile>true</spirit:volatile>
					</spirit:field>
				</spirit:register>
				<spirit:register>
					<spirit:name>count</spirit:name>
					<spirit:description>Current counter value</spirit:description>
					<spirit:addressOffset>0x8</spirit:addressOffset>
					<spirit:size>32</spirit:size>
					<spirit:volatile>true</spirit:volatile>
					<spirit:access>read-write</spirit:access>
					<spirit:reset>
						<spirit:value>0x0</spirit:value>
					</spirit:reset>
          <spirit:field>
            <spirit:name>value</spirit:name>
            <spirit:description>The current counter value</spirit:description>
            <spirit:bitOffset>0</spirit:bitOffset>
            <spirit:bitWidth>32</spirit:bitWidth>
            <spirit:volatile>true</spirit:volatile>
          </spirit:field>									
				</spirit:register>
			</spirit:addressBlock>
			<spirit:addressBlock>
				<spirit:name>fifo0</spirit:name>
				<spirit:baseAddress>0x2000</spirit:baseAddress>
				<spirit:range>64</spirit:range>
				<spirit:width>32</spirit:width>
				<spirit:register>
					<spirit:name>control</spirit:name>
					<spirit:addressOffset>0x0</spirit:addressOffset>
					<spirit:size>32</spirit:size>
					<spirit:access>read-write</spirit:access>			
          <spirit:field>
            <spirit:name>flush</spirit:name>
            <spirit:description>Empties the FIFO</spirit:description>
            <spirit:bitOffset>0</spirit:bitOffset>
            <spirit:bitWidth>1</spirit:bitWidth>
          </spirit:field>         					
				</spirit:register>
        <spirit:register>
          <spirit:name>status</spirit:name>
          <spirit:description>Status register</spirit:description>
          <spirit:addressOffset>0x4</spirit:addressOffset>
          <spirit:size>32</spirit:size>
          <spirit:volatile>true</spirit:volatile>
          <spirit:access>read-only</spirit:access>         
          <spirit:field>
            <spirit:name>fifo_empty</spirit:name>
            <spirit:description>FIFO Empty flag</spirit:description>
            <spirit:bitOffset>8</spirit:bitOffset>
            <spirit:bitWidth>1</spirit:bitWidth>
          <spirit:volatile>true</spirit:volatile>
          </spirit:field>
          <spirit:field>
            <spirit:name>fifo_level</spirit:name>
            <spirit:description>Amount of data currently in the FIFO</spirit:description>
            <spirit:bitOffset>0</spirit:bitOffset>
            <spirit:bitWidth>8</spirit:bitWidth>
          <spirit:volatile>true</spirit:volatile>
          </spirit:field>  
          <spirit:field>
            <spirit:name>fifo_full</spirit:name>
            <spirit:description>FIFO Full flag</spirit:description>
            <spirit:bitOffset>9</spirit:bitOffset>
            <spirit:bitWidth>1</spirit:bitWidth>
          <spirit:volatile>true</spirit:volatile>
          </spirit:field>               
          <spirit:field>
            <spirit:name>fifo_overflow</spirit:name>
            <spirit:description>FIFO Overflow flag</spirit:description>
            <spirit:bitOffset>11</spirit:bitOffset>
            <spirit:bitWidth>1</spirit:bitWidth>
          <spirit:volatile>true</spirit:volatile>
          </spirit:field>               
          <spirit:field>
            <spirit:name>fifo_underflow</spirit:name>
            <spirit:description>FIFO Underflow flag</spirit:description>
            <spirit:bitOffset>10</spirit:bitOffset>
            <spirit:bitWidth>1</spirit:bitWidth>
          <spirit:volatile>true</spirit:volatile>
          </spirit:field>               
        </spirit:register>
        <spirit:register>
          <spirit:name>data_in</spirit:name>
          <spirit:addressOffset>0x8</spirit:addressOffset>
          <spirit:size>32</spirit:size>
          <spirit:access>write-only</spirit:access>
          <spirit:field>
            <spirit:name>data_in</spirit:name>
            <spirit:description>Write register for fifo data</spirit:description>
            <spirit:bitOffset>0</spirit:bitOffset>
            <spirit:bitWidth>32</spirit:bitWidth>
            <spirit:volatile>true</spirit:volatile>
          </spirit:field>             
        </spirit:register>
        <spirit:register>
          <spirit:name>data_out</spirit:name>
          <spirit:addressOffset>0xc</spirit:addressOffset>
          <spirit:size>32</spirit:size>
          <spirit:volatile>true</spirit:volatile>
          <spirit:access>read-only</spirit:access>
          <spirit:field>
            <spirit:name>data_out</spirit:name>
            <spirit:description>Fifo output register</spirit:description>
            <spirit:bitOffset>0</spirit:bitOffset>
            <spirit:bitWidth>32</spirit:bitWidth>
            <spirit:volatile>true</spirit:volatile>
          </spirit:field>            
        </spirit:register>
			</spirit:addressBlock>
		</spirit:memoryMap>
	</spirit:memoryMaps>
</spirit:component>
   
   




