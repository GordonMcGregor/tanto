package apb_test_pkg;
   
   import uvm_pkg::*;
   import apb_demo_pkg::*;

//*************************************//
//------------ BASE TEST --------------//
//*************************************//   
class apb_base_test extends uvm_test;
   `uvm_component_utils(apb_base_test)

   apb_env env;

   function new(string name, uvm_component parent);
      super.new(name, parent);
      uvm_reg::include_coverage("*", UVM_CVR_ALL);
   endfunction: new

   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      env = apb_env::type_id::create("env", this);
   endfunction: build_phase

   virtual task run_phase(uvm_phase phase);
      phase.phase_done.set_drain_time(this, 50);
      void'(env.regmodel.set_coverage(UVM_CVR_ALL));            
   endtask: run_phase
endclass: apb_base_test

//*************************************//
//-------------- TEST1 ----------------//
//*************************************//    

class test1 extends apb_base_test;
   `uvm_component_utils(test1)

   function new(string name, uvm_component parent);
      super.new(name, parent);
   endfunction: new

   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
   endfunction: build_phase

   virtual task run_phase(uvm_phase phase);
      uvm_reg_bit_bash_seq bash_seq;

      super.run_phase(phase);
      uvm_resource_db#(bit)::set({"REG::", env.regmodel.fifo0.status.get_full_name()}, "NO_REG_TESTS", 1, this);
      uvm_resource_db#(bit)::set({"REG::", env.regmodel.counter0.status.get_full_name()}, "NO_REG_TESTS", 1, this);      
      phase.raise_objection(this);
      #10;
      
      bash_seq = uvm_reg_bit_bash_seq::type_id::create("bash_seq", this);
      bash_seq.model = env.regmodel;
      bash_seq.model.reset();
      bash_seq.start(env.m_apb_agent.sqr);
      
      phase.drop_objection(this);
   endtask: run_phase
endclass: test1

//*************************************//
//-------------- TEST2 ----------------//
//*************************************//     

class test2 extends apb_base_test;
   `uvm_component_utils(test2)

   function new(string name, uvm_component parent);
      super.new(name, parent);
   endfunction: new

   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      
   endfunction: build_phase

   virtual task run_phase(uvm_phase phase);
      uvm_reg_hw_reset_seq reset_seq;

      super.run_phase(phase);
      uvm_resource_db#(bit)::set({"REG::", env.regmodel.fifo0.status.get_full_name()}, "NO_REG_TESTS", 1, this);
      uvm_resource_db#(bit)::set({"REG::", env.regmodel.counter0.status.get_full_name()}, "NO_REG_TESTS", 1, this);      
      phase.raise_objection(this);
      #10;
      
      reset_seq = uvm_reg_hw_reset_seq::type_id::create("reset_seq", this);
      reset_seq.model = env.regmodel;
      reset_seq.model.reset();
      reset_seq.start(env.m_apb_agent.sqr);
      
      phase.drop_objection(this);
   endtask: run_phase
endclass: test2

//*************************************//
//-------------- TEST3 ----------------//
//*************************************//     

class test3 extends apb_base_test;
   `uvm_component_utils(test3)

   function new(string name, uvm_component parent);
      super.new(name, parent);
   endfunction: new

   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);

      
   endfunction: build_phase

   virtual task run_phase(uvm_phase phase);

      uvm_status_e status;
      uvm_reg_data_t data;

      super.run_phase(phase);
	
      phase.raise_objection(this);
      `uvm_info("TEST3","Register Write...", UVM_NONE);
      #10
      // Write FIFO0 Control Register to enable Flush
      env.regmodel.fifo0.control.reset();
      env.regmodel.fifo0.control.write(status, 32'h0000BEEF);
      // Read FIFO0 Control Register and check 
      env.regmodel.fifo0.control.read(status, data);
      if(data !== 32'h00000001)
	`uvm_error("TEST3", $sformatf("Mismatch when reading fifo0.control after reset: h%0h instead if 'h00000001", data));
      // Write FIFO0 Control Register to disable Flush
      env.regmodel.fifo0.control.write(status, 32'h00000000);      
      // Write 2 items to FIFO0
      env.regmodel.fifo0.data_in.reset();
      env.regmodel.fifo0.data_in.write(status, 32'h00000001);
      env.regmodel.fifo0.data_in.write(status, 32'h00000002);      

      // Read 2 items from FIFO0 and check
      env.regmodel.fifo0.data_out.read(status, data);
      if(data !== 32'h00000001)
	`uvm_error("TEST3", $sformatf("Mismatch when reading fifo0.data_out: h%0h instead if 'h00000001", data));

      env.regmodel.fifo0.data_out.read(status, data);      
      if(data !== 32'h00000002)
	`uvm_error("TEST3", $sformatf("Mismatch when reading fifo0.data_out: h%0h instead if 'h00000002", data));      
      
      phase.drop_objection(this);
   endtask: run_phase
endclass: test3
   
endpackage: apb_test_pkg
   