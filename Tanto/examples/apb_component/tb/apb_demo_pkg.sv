`ifndef APB_DEMO_PKG__SV
`define APB_DEMO_PKG__SV

package apb_demo_pkg;

   import apb_pkg::*;
   
`include "../../output/uvm/apb_demo_uvm.sv"
`include "apb_env.sv"
   
endpackage: apb_demo_pkg
`endif   