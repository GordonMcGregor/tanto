'''
Created on Jun 28, 2012

Copyright 2012 Verilab, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
@author: gmcgregor
'''

from xml.etree import ElementTree as ET
from logging import info, debug, warning
from mako.template import Template
from tanto.tanto import TantoParser
import sys
import string
import Tkinter

class RalfParser(TantoParser):
        
    def access(self, *args):
        print "Saw access call:"
        print args[0]
    
    def bits(self, *args):
        print "Saw bits call:"
        print args[0]
    
    def block(self, *args):
        print "Saw block call:"
        print args[0]
        print args[1]
        self.tcl.eval(args[2])
    
    def bytes(self, *args):
        print "Saw bytes call:"
        print args[0]
    
    def constraint(self, *args):
        print "Saw constraint call:"
        print args[0]
    
    def doc(self, *args):
        print "Saw doc call:"
        print args[0]
    
    def domain(self, *args):
        print "Saw domain call:"
        print args[0]
    
    def endian(self, *args):
        print "Saw endian call:"
        print args[0]
    
    def field(self, *args):
        print "Saw field call:"
        print args[0]
        self.tcl.eval(args[-1])

    def hard_reset(self, *args):
        print "Saw hard_reset call:"
        print args[0]
    
    def hdl_path(self, *args):
        print "Saw hdl_path call:"
        print args[0]
    
    def initial(self, *args):
        print "Saw initial call:"
        print args[0]
    
    def left_to_right(self, *args):
        print "Saw left_to_right call:"
        print args[0]
    
    def memory(self, *args):
        print "Saw memory call:"
        print args[0]
    
    def noise(self, *args):
        print "Saw noise call:"
        print args[0]
    
    def read(self, *args):
        print "Saw read call:"
        print args[0]
    
    def regfile(self, *args):
        print "Saw regfile call:"
        print args[0]
    
    def register(self, *args):
        print "Saw register call:"
        print args[0]
        print args[1]
        self.tcl.eval(args[2])
    
    def reset(self, *args):
        print "Saw reset call:"
        print args[0]
    
    def shared(self, *args):
        print "Saw shared call:"
        print args[0]
    
    def size(self, *args):
        print "Saw size call:"
        print args[0]
    
    def soft_reset(self, *args):
        print "Saw soft_reset call:"
        print args[0]
        
    def system(self, *args):
        print "Saw system call:"
        print args[0]
        self.tcl.eval(args[1])
        
    def write(self, *args):
        print "Saw write call:"
        print args[0]
        
    def parse(self):
        '''RALF parsing goes here'''
        filename = self.parent.config.get('tanto', 'source_directory')+self.parent.config.get('tanto', 'top')+'.ralf'
        debug('Parsing RALF file: %s'%  filename)

        result = []
        self.tcl = Tkinter.Tcl()
        self.tcl.createcommand('access', self.access)
        self.tcl.createcommand('bits', self.bits)
        self.tcl.createcommand('block', self.block)
        self.tcl.createcommand('bytes', self.bytes)
        self.tcl.createcommand('constraint', self.constraint)
        self.tcl.createcommand('doc', self.doc)
        self.tcl.createcommand('domain', self.domain)
        self.tcl.createcommand('endian', self.endian)
        self.tcl.createcommand('field', self.field)
        self.tcl.createcommand('hard_reset', self.hard_reset)
        self.tcl.createcommand('hdl_path', self.hdl_path)
        self.tcl.createcommand('initial', self.initial)
        self.tcl.createcommand('left_to_right', self.left_to_right)
        self.tcl.createcommand('memory', self.memory)
        self.tcl.createcommand('noise', self.noise)
        self.tcl.createcommand('read', self.read)
        self.tcl.createcommand('regfile', self.regfile)
        self.tcl.createcommand('register', self.register)
        self.tcl.createcommand('reset', self.reset)
        self.tcl.createcommand('shared', self.shared)
        self.tcl.createcommand('size', self.size)
        self.tcl.createcommand('soft_reset', self.soft_reset)
        self.tcl.createcommand('system', self.system)
        self.tcl.createcommand('write', self.write)
        
        self.tcl.eval('source {%s}' % filename)

        sys.exit()