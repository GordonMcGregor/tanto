'''
Created on Jun 28, 2012

Copyright 2012 Verilab, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
@author: gmcgregor
'''

from tanto.tanto import TantoRenderer

from logging import info, debug, warning
from mako.template import Template
import shutil
import os.path

class HtmlRenderer(TantoRenderer):
    
    def render(self, template):
        template_name = '%s/%s.template' % (template, template)
        info('Rendering %s using custom renderer' % (template))
        debug('Template %s' % (template_name))
        TantoRenderer.render(self, template)
        shutil.copy(os.path.join( os.path.dirname(os.path.realpath(__file__)), self.parent.config.get(template, 'css_file')), self.output_path)
        
